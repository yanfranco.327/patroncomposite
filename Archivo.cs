﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronComposite
{
    public class Archivo : Componente
    {
        int _tamaño; //Para un atributo interno
        public Archivo(string nombre, int tamaño) : base(nombre) //Nombre va a ir a la clase  padre -componente.
        {
            _tamaño = tamaño; //Para un atributo interno
        }
        public int Tamaño { get { return _tamaño; } }  //Esto va ser accedido de prpoiedad  para calcular el tamaño.
      
       
        
        
        //-----------------Esto es herencia de la clase componente.
        //....Hojas delk arbol.
        
        public override void AgregarHijo(Componente c)
        {

        }

        public override IList<Componente> ObtenerHijos()
        {
            return null;
        }


        //----Nos devuelve el tamañano de la hoja.
        public override int ObtenerTamaño
        {
            get
            {
                return _tamaño;
            }
        }
    }
}
