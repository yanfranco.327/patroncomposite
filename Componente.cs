﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronComposite
{
    //clases abstractas aquellas clases base (superclases) de las que no se permite la creación de objetos
    public abstract class Componente //Una clase publica abstracta 
    {

        string _nombre;

        public Componente(string nombre) //Al crear un atributo es nesesario el atributo nombre 
        {
            _nombre = nombre;
        }

        public string Nombre { get { return _nombre; } } //Aqui es accedido  y nos devuelve el nombre 
        public abstract void AgregarHijo(Componente c); //operaciones para  AgregarHijo
        public abstract IList<Componente> ObtenerHijos(); //operaciones para  ObtenerHijos
        public abstract int ObtenerTamaño { get; }//operaciones para  ObtenerTamaño
    }
}
