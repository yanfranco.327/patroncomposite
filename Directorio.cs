﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronComposite
{
    public class Directorio : Componente //Directorio es una implementacion de componente
    {
        private List<Componente> _hijos; //Aqui vemos una lista de de componente .
        public Directorio(string nombre) : base(nombre)  //El directorio , con su nombre 
        {
            _hijos = new List<Componente>(); //Instanciar la lista de hijo 
        }

        public override void AgregarHijo(Componente c)
        {
            _hijos.Add(c);  //Del parametro hijo .Sera agragado a la lista de hijos.
        }

        public override IList<Componente> ObtenerHijos()
        {
            return _hijos.ToArray();  //Ilist :devuelve una copia de hijos 
        }

        public override int ObtenerTamaño //Implementado apartir de un get.
        {
            get
            {
                int t = 0; //Para acomular el tamaño de todos los hijos.

                foreach (var item in _hijos) //consulta el tamaño de los hijos de tipo componente.
                {
                    t += item.ObtenerTamaño;  //Tamaño individual.
                }

                return t;
            }

        }
    }
}
